<?php
/**
* Implements page callback
*/
function reporting_cloud_templates_overview() {
  $page = array();
  $credentials = variable_get('reporting_cloud', array('username' => NULL, 'password' => NULL, 'templates' => array()));
  if (empty($credentials['username']) || empty($credentials['password'])) {
    drupal_set_message(t('Setup reporting.cloud credentials to setup templates.'), 'error');
    return $page;
  }
  
  $page[] = drupal_get_form('reporting_cloud_template_list_form');
  $page[] = drupal_get_form('reporting_cloud_template_upload_form');
  return $page;
}

function reporting_cloud_template_list_form($form, &$form_state) {
  $instance = reporting_cloud_get_instance();
  $templates = $instance->getTemplateList();
  $template_options = array();
  foreach ($templates as $template) {
    $template_options[$template['template_name']] = $template['template_name'];
  }
  
  $form['templates'] = array(
    '#title' => t('Select available templates'),
    '#type' => 'checkboxes',
    '#options' => $template_options,
    '#default_value' => (empty($credentials['templates'])) ? array_keys($template_options) : $credentials['templates'],
    '#required' => TRUE,
   );
   
   $form['actions'] = array(
     '#type' => 'actions',
   );
   
   $form['actions']['submit'] = array(
     '#type' => 'submit',
     '#value' => t('Save'),
   );
   
   return $form;
}

function reporting_cloud_template_list_form_submit($form, &$form_state) {
  $reporting_cloud_config = variable_get('reporting_cloud', array());
  $reporting_cloud_config['templates'] = $form_state['values']['templates'];
  variable_set('reporting_cloud', $reporting_cloud_config);
  $rc_instance = reporting_cloud_get_instance();
  $templates_metadata = array();
  foreach ($form_state['values']['templates'] as $template_name => $template_name_val) {
    db_merge('reporting_cloud_template')->key(array('name' => $template_name))
      ->fields(array(
         'name' => $template_name,
         'metadata' => serialize($rc_instance->getTemplateInfo($template_name)),
        ))
      ->execute();
  }
  
  if (!empty($form_state['values']['templates'])) {
    $delete = db_delete('reporting_cloud_template');
    $delete->condition('name', $form_state['values']['templates'], 'NOT IN');
    $delete->execute();
  }
  
  drupal_set_message(t('Your changes have been saved.'));
}

function reporting_cloud_template_upload_form($form, &$form_state) {
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('Upload template'),
    '#description' => t('You can upload your templates to reporting.cloud using this form. Allowed file types doc, docx, rtf, tx.'),
  );
  
  $form['ovverite'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ovverite template if exists.'),
    '#default_value' => 0,
  );
  
   $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );
  
  return $form;
}

function reporting_cloud_template_upload_form_validate($form, &$form_state) {
  $validators = array('file_validate_extensions' => array('doc docx rtf tx'));
  $file = file_save_upload('file', $validators, 'temporary://', FILE_EXISTS_REPLACE);
  if ($file) {
    $rc_instance = reporting_cloud_get_instance();
    if ($rc_instance->templateExists(drupal_basename($file->uri)) && !$form_state['values']['ovverite']) {
      form_set_error('file', t('Template with same name already exists.'));
    } else {
      $form_state['storage']['file'] = $file;
    }
  }
}

function reporting_cloud_template_upload_form_submit($form, &$form_state) {
  $file = $form_state['storage']['file'];
  
  if ($file) {
     $rc_instance = reporting_cloud_get_instance();
     $status = $rc_instance->uploadTemplate(drupal_realpath($file->uri));
     if ($status) {
       drupal_set_message(t('"%template" successfully uploaded.', array('%template' => check_plain(drupal_basename($file->uri)))));
     }
  }
  
}
